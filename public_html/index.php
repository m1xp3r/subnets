<!DOCTYPE html>
<html lang="en-ca">
<head>
    <title>Subnet Mask Cheat Sheet</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <!-- CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/default.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CJosefin+Sans:700"
          rel="stylesheet">
</head>

<body>
<!-- Nav -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand text-center" href="/"><strong>Subnet Cheat Sheet</strong>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-nav-dropdown"
                aria-controls="navbar-nav-dropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar-nav-dropdown">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#ipv4"><i class="fas fa-network-wired" aria-hidden="true"></i> IPv4</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://www.ietf.org/rfc/rfc1878.txt"><i class="fas fa-file"
                                                                                      aria-hidden="true"></i> RFC
                        1878</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#ipv6"><i class="fas fa-network-wired" aria-hidden="true"></i> IPv6</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://www.ietf.org/rfc/rfc3513.txt"><i class="fas fa-file"
                                                                                       aria-hidden="true"></i> RFC
                        3513</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<section id="ipv4" class="container page-start">
    <div class="row">
        <div class="col">
            <h1 class="page-header text-center">IPv4</h1>
        </div>
    </div>
    <div class="row">
        <div class="col mx-auto contact text-center">
            <h2>Subnets</h2>
            <table class="chart">
                <tr>
                    <th></th>
                    <th>Addresses</th>
                    <th>Hosts</th>
                    <th>Netmask</th>
                    <th>Amount of a Class C</th>
                </tr>
                <tr align="center">
                    <th>/30</th>
                    <td>4</td>
                    <td>2</td>
                    <td>255.255.255.252</td>
                    <td>1/64</td>
                </tr>
                <tr align="center">
                    <th>/29</th>
                    <td>8</td>
                    <td>6</td>
                    <td>255.255.255.248</td>
                    <td>1/32</td>
                </tr>
                <tr align="center">
                    <th>/28</th>
                    <td>16</td>
                    <td>14</td>
                    <td>255.255.255.240</td>
                    <td>1/16</td>
                </tr>
                <tr align="center">
                    <th>/27</th>
                    <td>32</td>
                    <td>30</td>
                    <td>255.255.255.224</td>
                    <td>1/8</td>
                </tr>
                <tr align="center">
                    <th>/26</th>
                    <td>64</td>
                    <td>62</td>
                    <td>255.255.255.192</td>
                    <td>1/4</td>
                </tr>
                <tr align="center">
                    <th>/25</th>
                    <td>128</td>
                    <td>126</td>
                    <td>255.255.255.128</td>
                    <td>1/2</td>
                </tr>
                <tr align="center">
                    <th>/24</th>
                    <td>256</td>
                    <td>254</td>
                    <td>255.255.255.0</td>
                    <td>1</td>
                </tr>
                <tr align="center">
                    <th>/23</th>
                    <td>512</td>
                    <td>510</td>
                    <td>255.255.254.0</td>
                    <td>2</td>
                </tr>
                <tr align="center">
                    <th>/22</th>
                    <td>1024</td>
                    <td>1022</td>
                    <td>255.255.252.0</td>
                    <td>4</td>
                </tr>
                <tr align="center">
                    <th>/21</th>
                    <td>2048</td>
                    <td>2046</td>
                    <td>255.255.248.0</td>
                    <td>8</td>
                </tr>
                <tr align="center">
                    <th>/20</th>
                    <td>4096</td>
                    <td>4094</td>
                    <td>255.255.240.0</td>
                    <td>16</td>
                </tr>
                <tr align="center">
                    <th>/19</th>
                    <td>8192</td>
                    <td>8190</td>
                    <td>255.255.224.0</td>
                    <td>32</td>
                </tr>
                <tr align="center">
                    <th>/18</th>
                    <td>16384</td>
                    <td>16382</td>
                    <td>255.255.192.0</td>
                    <td>64</td>
                </tr>
                <tr align="center">
                    <th>/17</th>
                    <td>32768</td>
                    <td>32766</td>
                    <td>255.255.128.0</td>
                    <td>128</td>
                </tr>
                <tr align="center">
                    <th>/16</th>
                    <td>65536</td>
                    <td>65534</td>
                    <td>255.255.0.0</td>
                    <td>256</td>
                </tr>
            </table>
            <hr>
            <div>
                <h2>Private Address Blocks</h2>
                <p>10.0.0.0 to 10.255.255.255 (<code>10.0.0.0/8</code>)</p>
                <p>172.16.0.0 to 172.31.255.255 (<code>172.16.0.0/12</code>)</p>
                <p>192.168.0.0 to 192.168.255.255 (<code>192.168.0.0/16</code>)</p>
            </div>
            <div>
                <h2>Link Local Addresses</h2>
                <p>169.254.0.0 to 169.254.255.255 (<code>169.254.0.0/16</code>)</p>
            </div>
            <div>
                <h2>TEST-NET Addresses</h2>
                <p>192.0.2.0 to 192.0.2.255 (<code>192.0.2.0/24</code>)</p>
            </div>
        </div>
        <div class="col mx-auto contact text-center">
            <h2>Sub-class C blocks</h2>
            <table class="layout">
                <tr valign="top">
                    <td>
                        <p class='desc'>/25 -- 2 Subnets -- 126 Hosts/Subnet</p>

                        <table class="chart">
                            <tr>
                                <th>Network #</th>
                                <th>IP Range</th>
                                <th>Broadcast</th>
                            </tr>
                            <tr>
                                <td>.0</td>
                                <td>.1-.126</td>
                                <td>.127</td>
                            </tr>
                            <tr>
                                <td>.128</td>
                                <td>.129-.254</td>
                                <td>.255</td>
                            </tr>
                        </table>
                    </td>

                    <td rowspan="5">
                        <p class='desc'>/30 -- 64 Subnets -- 2 Hosts/Subnet</p>

                        <table class="chart">
                            <tr>
                                <th>Network #</th>
                                <th>IP Range</th>
                                <th>Broadcast</th>
                            </tr>
                            <tr>
                                <td>.0</td>
                                <td>.1-.2</td>
                                <td>.3</td>
                            </tr>
                            <tr>
                                <td>.4</td>
                                <td>.5-.6</td>
                                <td>.7</td>
                            </tr>
                            <tr>
                                <td>.8</td>
                                <td>.9-.10</td>
                                <td>.11</td>
                            </tr>
                            <tr>
                                <td>.12</td>
                                <td>.13-.14</td>
                                <td>.15</td>
                            </tr>
                            <tr>
                                <td>.16</td>
                                <td>.17-.18</td>
                                <td>.19</td>
                            </tr>
                            <tr>
                                <td>.20</td>
                                <td>.21-.22</td>
                                <td>.23</td>
                            </tr>
                            <tr>
                                <td>.24</td>
                                <td>.25-.26</td>
                                <td>.27</td>
                            </tr>
                            <tr>
                                <td>.28</td>
                                <td>.29-.30</td>
                                <td>.31</td>
                            </tr>
                            <tr>
                                <td>.32</td>
                                <td>.33-.34</td>
                                <td>.35</td>
                            </tr>
                            <tr>
                                <td>.36</td>
                                <td>.37-.38</td>
                                <td>.39</td>
                            </tr>
                            <tr>
                                <td>.40</td>
                                <td>.41-.42</td>
                                <td>.43</td>
                            </tr>
                            <tr>
                                <td>.44</td>
                                <td>.45-.46</td>
                                <td>.47</td>
                            </tr>
                            <tr>
                                <td>.48</td>
                                <td>.49-.50</td>
                                <td>.51</td>
                            </tr>
                            <tr>
                                <td>.52</td>
                                <td>.53-.54</td>
                                <td>.55</td>
                            </tr>
                            <tr>
                                <td>.56</td>
                                <td>.57-.58</td>
                                <td>.59</td>
                            </tr>
                            <tr>
                                <td>.60</td>
                                <td>.61-.62</td>
                                <td>.63</td>
                            </tr>
                            <tr>
                                <td>.64</td>
                                <td>.65-.66</td>
                                <td>.67</td>
                            </tr>
                            <tr>
                                <td>.68</td>
                                <td>.69-.70</td>
                                <td>.71</td>
                            </tr>
                            <tr>
                                <td>.72</td>
                                <td>.73-.74</td>
                                <td>.75</td>
                            </tr>
                            <tr>
                                <td>.76</td>
                                <td>.77-.78</td>
                                <td>.79</td>
                            </tr>
                            <tr>
                                <td>.80</td>
                                <td>.81-.82</td>
                                <td>.83</td>
                            </tr>
                            <tr>
                                <td>.84</td>
                                <td>.85-.86</td>
                                <td>.87</td>
                            </tr>
                            <tr>
                                <td>.88</td>
                                <td>.89-.90</td>
                                <td>.91</td>
                            </tr>
                            <tr>
                                <td>.92</td>
                                <td>.93-.94</td>
                                <td>.95</td>
                            </tr>
                            <tr>
                                <td>.96</td>
                                <td>.97-.98</td>
                                <td>.99</td>
                            </tr>
                            <tr>
                                <td>.100</td>
                                <td>.101-.102</td>
                                <td>.103</td>
                            </tr>
                            <tr>
                                <td>.104</td>
                                <td>.105-.106</td>
                                <td>.107</td>
                            </tr>
                            <tr>
                                <td>.108</td>
                                <td>.109-.110</td>
                                <td>.111</td>
                            </tr>
                            <tr>
                                <td>.112</td>
                                <td>.113-.114</td>
                                <td>.115</td>
                            </tr>
                            <tr>
                                <td>.116</td>
                                <td>.117-.118</td>
                                <td>.119</td>
                            </tr>
                            <tr>
                                <td>.120</td>
                                <td>.121-.122</td>
                                <td>.123</td>
                            </tr>
                            <tr>
                                <td>.124</td>
                                <td>.125-.126</td>
                                <td>.127</td>
                            </tr>
                            <tr>
                                <td>.128</td>
                                <td>.129-.130</td>
                                <td>.131</td>
                            </tr>
                            <tr>
                                <td>.132</td>
                                <td>.133-.134</td>
                                <td>.135</td>
                            </tr>
                            <tr>
                                <td>.136</td>
                                <td>.137-.138</td>
                                <td>.139</td>
                            </tr>
                            <tr>
                                <td>.140</td>
                                <td>.141-.142</td>
                                <td>.143</td>
                            </tr>
                            <tr>
                                <td>.144</td>
                                <td>.145-.146</td>
                                <td>.147</td>
                            </tr>
                            <tr>
                                <td>.148</td>
                                <td>.149-.150</td>
                                <td>.151</td>
                            </tr>
                            <tr>
                                <td>.152</td>
                                <td>.153-.154</td>
                                <td>.155</td>
                            </tr>
                            <tr>
                                <td>.156</td>
                                <td>.157-.158</td>
                                <td>.159</td>
                            </tr>
                            <tr>
                                <td>.160</td>
                                <td>.161-.162</td>
                                <td>.163</td>
                            </tr>
                            <tr>
                                <td>.164</td>
                                <td>.165-.166</td>
                                <td>.167</td>
                            </tr>
                            <tr>
                                <td>.168</td>
                                <td>.169-.170</td>
                                <td>.171</td>
                            </tr>
                            <tr>
                                <td>.172</td>
                                <td>.173-.174</td>
                                <td>.175</td>
                            </tr>
                            <tr>
                                <td>.176</td>
                                <td>.177-.178</td>
                                <td>.179</td>
                            </tr>
                            <tr>
                                <td>.180</td>
                                <td>.181-.182</td>
                                <td>.183</td>
                            </tr>
                            <tr>
                                <td>.184</td>
                                <td>.185-.186</td>
                                <td>.187</td>
                            </tr>
                            <tr>
                                <td>.188</td>
                                <td>.189-.190</td>
                                <td>.191</td>
                            </tr>
                            <tr>
                                <td>.192</td>
                                <td>.193-.194</td>
                                <td>.195</td>
                            </tr>
                            <tr>
                                <td>.196</td>
                                <td>.197-.198</td>
                                <td>.199</td>
                            </tr>
                            <tr>
                                <td>.200</td>
                                <td>.201-.202</td>
                                <td>.203</td>
                            </tr>
                            <tr>
                                <td>.204</td>
                                <td>.205-.206</td>
                                <td>.207</td>
                            </tr>
                            <tr>
                                <td>.208</td>
                                <td>.209-.210</td>
                                <td>.211</td>
                            </tr>
                            <tr>
                                <td>.212</td>
                                <td>.213-.214</td>
                                <td>.215</td>
                            </tr>
                            <tr>
                                <td>.216</td>
                                <td>.217-.218</td>
                                <td>.219</td>
                            </tr>
                            <tr>
                                <td>.220</td>
                                <td>.221-.222</td>
                                <td>.223</td>
                            </tr>
                            <tr>
                                <td>.224</td>
                                <td>.225-.226</td>
                                <td>.227</td>
                            </tr>
                            <tr>
                                <td>.228</td>
                                <td>.229-.230</td>
                                <td>.231</td>
                            </tr>
                            <tr>
                                <td>.232</td>
                                <td>.233-.234</td>
                                <td>.235</td>
                            </tr>
                            <tr>
                                <td>.236</td>
                                <td>.237-.238</td>
                                <td>.239</td>
                            </tr>
                            <tr>
                                <td>.240</td>
                                <td>.241-.242</td>
                                <td>.243</td>
                            </tr>
                            <tr>
                                <td>.244</td>
                                <td>.245-.246</td>
                                <td>.247</td>
                            </tr>
                            <tr>
                                <td>.248</td>
                                <td>.249-.250</td>
                                <td>.251</td>
                            </tr>
                            <tr>
                                <td>.252</td>
                                <td>.253-.254</td>
                                <td>.255</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p class='desc'>/26 -- 4 Subnets -- 62 Hosts/Subnet</p>
                        <table class="chart">
                            <tr>
                                <th>Network #</th>
                                <th>IP Range</th>
                                <th>Broadcast</th>
                            </tr>
                            <tr>
                                <td>.0</td>
                                <td>.1-.62</td>
                                <td>.63</td>
                            </tr>
                            <tr>
                                <td>.64</td>
                                <td>.65-.126</td>
                                <td>.127</td>
                            </tr>
                            <tr>
                                <td>.128</td>
                                <td>.129-.190</td>
                                <td>.191</td>
                            </tr>
                            <tr>
                                <td>.192</td>
                                <td>.193-.254</td>
                                <td>.255</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p class='desc'>/27 -- 8 Subnets -- 30 Hosts/Subnet</p>
                        <table class="chart">
                            <tr>
                                <th>Network #</th>
                                <th>IP Range</th>
                                <th>Broadcast</th>
                            </tr>
                            <tr>
                                <td>.0</td>
                                <td>.1-.30</td>
                                <td>.31</td>
                            </tr>
                            <tr>
                                <td>.32</td>
                                <td>.33-.62</td>
                                <td>.63</td>
                            </tr>
                            <tr>
                                <td>.64</td>
                                <td>.65-.94</td>
                                <td>.95</td>
                            </tr>
                            <tr>
                                <td>.96</td>
                                <td>.97-.126</td>
                                <td>.127</td>
                            </tr>
                            <tr>
                                <td>.128</td>
                                <td>.129-.158</td>
                                <td>.159</td>
                            </tr>
                            <tr>
                                <td>.160</td>
                                <td>.161-.190</td>
                                <td>.191</td>
                            </tr>
                            <tr>
                                <td>.192</td>
                                <td>.193-.222</td>
                                <td>.223</td>
                            </tr>
                            <tr>
                                <td>.224</td>
                                <td>.225-.254</td>
                                <td>.255</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p class='desc'>/28 -- 16 Subnets -- 14 Hosts/Subnet</p>
                        <table class="chart">
                            <tr>
                                <th>Network #</th>
                                <th>IP Range</th>
                                <th>Broadcast</th>
                            </tr>
                            <tr>
                                <td>.0</td>
                                <td>.1-.14</td>
                                <td>.15</td>
                            </tr>
                            <tr>
                                <td>.16</td>
                                <td>.17-.30</td>
                                <td>.31</td>
                            </tr>
                            <tr>
                                <td>.32</td>
                                <td>.33-.46</td>
                                <td>.47</td>
                            </tr>
                            <tr>
                                <td>.48</td>
                                <td>.49-.62</td>
                                <td>.63</td>
                            </tr>
                            <tr>
                                <td>.64</td>
                                <td>.65-.78</td>
                                <td>.79</td>
                            </tr>
                            <tr>
                                <td>.80</td>
                                <td>.81-.94</td>
                                <td>.95</td>
                            </tr>
                            <tr>
                                <td>.96</td>
                                <td>.97-.110</td>
                                <td>.111</td>
                            </tr>
                            <tr>
                                <td>.112</td>
                                <td>.113-.126</td>
                                <td>.127</td>
                            </tr>
                            <tr>
                                <td>.128</td>
                                <td>.129-.142</td>
                                <td>.143</td>
                            </tr>
                            <tr>
                                <td>.144</td>
                                <td>.145-.158</td>
                                <td>.159</td>
                            </tr>
                            <tr>
                                <td>.160</td>
                                <td>.161-.174</td>
                                <td>.175</td>
                            </tr>
                            <tr>
                                <td>.176</td>
                                <td>.177-.190</td>
                                <td>.191</td>
                            </tr>
                            <tr>
                                <td>.192</td>
                                <td>.193-.206</td>
                                <td>.207</td>
                            </tr>
                            <tr>
                                <td>.208</td>
                                <td>.209-.222</td>
                                <td>.223</td>
                            </tr>
                            <tr>
                                <td>.224</td>
                                <td>.225-.238</td>
                                <td>.239</td>
                            </tr>
                            <tr>
                                <td>.240</td>
                                <td>.241-.254</td>
                                <td>.255</td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p class='desc'>/29 -- 32 Subnets -- 6 Hosts/Subnet</p>
                        <table class="chart">
                            <tr>
                                <th>Network #</th>
                                <th>IP Range</th>
                                <th>Broadcast</th>
                            </tr>
                            <tr>
                                <td>.0</td>
                                <td>.1-.6</td>
                                <td>.7</td>
                            </tr>
                            <tr>
                                <td>.8</td>
                                <td>.9-.14</td>
                                <td>.15</td>
                            </tr>
                            <tr>
                                <td>.16</td>
                                <td>.17-.22</td>
                                <td>.23</td>
                            </tr>
                            <tr>
                                <td>.24</td>
                                <td>.25-.30</td>
                                <td>.31</td>
                            </tr>
                            <tr>
                                <td>.32</td>
                                <td>.33-.38</td>
                                <td>.39</td>
                            </tr>
                            <tr>
                                <td>.40</td>
                                <td>.41-.46</td>
                                <td>.47</td>
                            </tr>
                            <tr>
                                <td>.48</td>
                                <td>.49-.54</td>
                                <td>.55</td>
                            </tr>
                            <tr>
                                <td>.56</td>
                                <td>.57-.62</td>
                                <td>.63</td>
                            </tr>
                            <tr>
                                <td>.64</td>
                                <td>.65-.70</td>
                                <td>.71</td>
                            </tr>
                            <tr>
                                <td>.72</td>
                                <td>.73-.78</td>
                                <td>.79</td>
                            </tr>
                            <tr>
                                <td>.80</td>
                                <td>.81-.86</td>
                                <td>.87</td>
                            </tr>
                            <tr>
                                <td>.88</td>
                                <td>.89-.94</td>
                                <td>.95</td>
                            </tr>
                            <tr>
                                <td>.96</td>
                                <td>.97-.102</td>
                                <td>.103</td>
                            </tr>
                            <tr>
                                <td>.104</td>
                                <td>.105-.110</td>
                                <td>.111</td>
                            </tr>
                            <tr>
                                <td>.112</td>
                                <td>.113-.118</td>
                                <td>.119</td>
                            </tr>
                            <tr>
                                <td>.120</td>
                                <td>.121-.126</td>
                                <td>.127</td>
                            </tr>
                            <tr>
                                <td>.128</td>
                                <td>.129-.134</td>
                                <td>.135</td>
                            </tr>
                            <tr>
                                <td>.136</td>
                                <td>.137-.142</td>
                                <td>.143</td>
                            </tr>
                            <tr>
                                <td>.144</td>
                                <td>.145-.150</td>
                                <td>.151</td>
                            </tr>
                            <tr>
                                <td>.152</td>
                                <td>.153-.158</td>
                                <td>.159</td>
                            </tr>
                            <tr>
                                <td>.160</td>
                                <td>.161-.166</td>
                                <td>.167</td>
                            </tr>
                            <tr>
                                <td>.168</td>
                                <td>.169-.174</td>
                                <td>.175</td>
                            </tr>
                            <tr>
                                <td>.176</td>
                                <td>.177-.182</td>
                                <td>.183</td>
                            </tr>
                            <tr>
                                <td>.184</td>
                                <td>.185-.190</td>
                                <td>.191</td>
                            </tr>
                            <tr>
                                <td>.192</td>
                                <td>.193-.198</td>
                                <td>.199</td>
                            </tr>
                            <tr>
                                <td>.200</td>
                                <td>.201-.206</td>
                                <td>.207</td>
                            </tr>
                            <tr>
                                <td>.208</td>
                                <td>.209-.214</td>
                                <td>.215</td>
                            </tr>
                            <tr>
                                <td>.216</td>
                                <td>.217-.222</td>
                                <td>.223</td>
                            </tr>
                            <tr>
                                <td>.224</td>
                                <td>.225-.230</td>
                                <td>.231</td>
                            </tr>
                            <tr>
                                <td>.232</td>
                                <td>.233-.238</td>
                                <td>.239</td>
                            </tr>
                            <tr>
                                <td>.240</td>
                                <td>.241-.246</td>
                                <td>.247</td>
                            </tr>
                            <tr>
                                <td>.248</td>
                                <td>.249-.254</td>
                                <td>.255</td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>

        </div>
    </div>
</section>

<hr/>
<section id="ipv6" class="container page-start">
    <div class="row">
        <div class="col">
            <h1 class="page-header text-center">IPv6</h1>
        </div>
    </div>
    <div class="row">
        <div class="col mx-auto contact text-center">
            <table class="chart">
                <TR>
                    <TH>Subnet</TH>
                    <TH>Hosts</TH>
                    <TH>Amount of a /64</TH>
                </TR>
                <TR align=center>
                    <TH>/128</TH>
                    <TD>1</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/127</TH>
                    <TD>2</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/126</TH>
                    <TD>4</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/125</TH>
                    <TD>8</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/124</TH>
                    <TD>16</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/123</TH>
                    <TD>32</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/122</TH>
                    <TD>64</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/121</TH>
                    <TD>128</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/120</TH>
                    <TD>256</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/119</TH>
                    <TD>512</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/118</TH>
                    <TD>1,024</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/117</TH>
                    <TD>2,048</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/116</TH>
                    <TD>4,096</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/115</TH>
                    <TD>8,192</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/114</TH>
                    <TD>16,384</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/113</TH>
                    <TD>32,768</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/112</TH>
                    <TD>65,536</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/111</TH>
                    <TD>131,072</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/110</TH>
                    <TD>262,144</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/109</TH>
                    <TD>524,288</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/108</TH>
                    <TD>1,048,576</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/107</TH>
                    <TD>2,097,152</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/106</TH>
                    <TD>4,194,304</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/105</TH>
                    <TD>8,388,608</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/104</TH>
                    <TD>16,777,216</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/103</TH>
                    <TD>33,554,432</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/102</TH>
                    <TD>67,108,864</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/101</TH>
                    <TD>134,217,728</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/100</TH>
                    <TD>268,435,456</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/99</TH>
                    <TD>536,870,912</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/98</TH>
                    <TD>1,073,741,824</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/97</TH>
                    <TD>2,147,483,648</TD>
                    <TD></TD>
                </TR>
                <TR align=center bgcolor=yellow>
                    <TH>/96</TH>
                    <TD>4,294,967,296</TD>
                    <TD>This is equivalent to an IPv4 Internet or IPv4 /8</TD>
                </TR>
                <TR align=center>
                    <TH>/95</TH>
                    <TD>8,589,934,592</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/94</TH>
                    <TD>17,179,869,184</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/93</TH>
                    <TD>34,359,738,368</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/92</TH>
                    <TD>68,719,476,736</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/91</TH>
                    <TD>137,438,953,472</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/90</TH>
                    <TD>274,877,906,944</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/89</TH>
                    <TD>549,755,813,888</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/88</TH>
                    <TD>1,099,511,627,776</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/87</TH>
                    <TD>2,199,023,255,552</TD>
                    <TD>1/8,388,608</TD>
                </TR>
                <TR align=center>
                    <TH>/86</TH>
                    <TD>4,398,046,511,104</TD>
                    <TD>1/4,194,304</TD>
                </TR>
                <TR align=center>
                    <TH>/85</TH>
                    <TD>8,796,093,022,208</TD>
                    <TD>1/2,097,152</TD>
                </TR>
                <TR align=center>
                    <TH>/84</TH>
                    <TD>17,592,186,044,416</TD>
                    <TD>1/1,048,576</TD>
                </TR>
                <TR align=center>
                    <TH>/83</TH>
                    <TD>35,184,372,088,832</TD>
                    <TD>1/524,288</TD>
                </TR>
                <TR align=center>
                    <TH>/82</TH>
                    <TD>70,368,744,177,664</TD>
                    <TD>1/262,144</TD>
                </TR>
                <TR align=center>
                    <TH>/81</TH>
                    <TD>140,737,488,355,328</TD>
                    <TD>1/131,072</TD>
                </TR>
                <TR align=center>
                    <TH>/80</TH>
                    <TD>281,474,976,710,656</TD>
                    <TD>1/65,536</TD>
                </TR>
                <TR align=center>
                    <TH>/79</TH>
                    <TD>562,949,953,421,312</TD>
                    <TD>1/32,768</TD>
                </TR>
                <TR align=center>
                    <TH>/78</TH>
                    <TD>1,125,899,906,842,620</TD>
                    <TD>1/16,384</TD>
                </TR>
                <TR align=center>
                    <TH>/77</TH>
                    <TD>2,251,799,813,685,240</TD>
                    <TD>1/8,192</TD>
                </TR>
                <TR align=center>
                    <TH>/76</TH>
                    <TD>4,503,599,627,370,490</TD>
                    <TD>1/4,096</TD>
                </TR>
                <TR align=center>
                    <TH>/75</TH>
                    <TD>9,007,199,254,740,990</TD>
                    <TD>1/2,048</TD>
                </TR>
                <TR align=center>
                    <TH>/74</TH>
                    <TD>18,014,398,509,481,900</TD>
                    <TD>1/1,024</TD>
                </TR>
                <TR align=center>
                    <TH>/73</TH>
                    <TD>36,028,797,018,963,900</TD>
                    <TD>1/512</TD>
                </TR>
                <TR align=center>
                    <TH>/72</TH>
                    <TD>72,057,594,037,927,900</TD>
                    <TD>1/256</TD>
                </TR>
                <TR align=center>
                    <TH>/71</TH>
                    <TD>144,115,188,075,855,000</TD>
                    <TD>1/128</TD>
                </TR>
                <TR align=center>
                    <TH>/70</TH>
                    <TD>288,230,376,151,711,000</TD>
                    <TD>1/64</TD>
                </TR>
                <TR align=center>
                    <TH>/69</TH>
                    <TD>576,460,752,303,423,000</TD>
                    <TD>1/32</TD>
                </TR>
                <TR align=center>
                    <TH>/68</TH>
                    <TD>1,152,921,504,606,840,000</TD>
                    <TD>1/16</TD>
                </TR>
                <TR align=center>
                    <TH>/67</TH>
                    <TD>2,305,843,009,213,690,000</TD>
                    <TD>1/8</TD>
                </TR>
                <TR align=center>
                    <TH>/66</TH>
                    <TD>4,611,686,018,427,380,000</TD>
                    <TD>1/4</TD>
                </TR>
                <TR align=center>
                    <TH>/65</TH>
                    <TD>9,223,372,036,854,770,000</TD>
                    <TD>1/2</TD>
                </TR>
                <TR align=center bgcolor=yellow>
                    <TH>/64</TH>
                    <TD>18,446,744,073,709,500,000</TD>
                    <TD>This is the standard end user allocation</TD>
                </TR>
                <TR align=center>
                    <TH>/63</TH>
                    <TD>36,893,488,147,419,100,000</TD>
                    <TD>2</TD>
                </TR>
                <TR align=center>
                    <TH>/62</TH>
                    <TD>73,786,976,294,838,200,000</TD>
                    <TD>4</TD>
                </TR>
                <TR align=center>
                    <TH>/61</TH>
                    <TD>147,573,952,589,676,000,000</TD>
                    <TD>8</TD>
                </TR>
                <TR align=center>
                    <TH>/60</TH>
                    <TD>295,147,905,179,352,000,000</TD>
                    <TD>16</TD>
                </TR>
                <TR align=center>
                    <TH>/59</TH>
                    <TD>590,295,810,358,705,000,000</TD>
                    <TD>32</TD>
                </TR>
                <TR align=center>
                    <TH>/58</TH>
                    <TD>1,180,591,620,717,410,000,000</TD>
                    <TD>64</TD>
                </TR>
                <TR align=center>
                    <TH>/57</TH>
                    <TD>2,361,183,241,434,820,000,000</TD>
                    <TD>128</TD>
                </TR>
                <TR align=center>
                    <TH>/56</TH>
                    <TD>4,722,366,482,869,640,000,000</TD>
                    <TD>256</TD>
                </TR>
                <TR align=center>
                    <TH>/55</TH>
                    <TD>9,444,732,965,739,290,000,000</TD>
                    <TD>512</TD>
                </TR>
                <TR align=center>
                    <TH>/54</TH>
                    <TD>18,889,465,931,478,500,000,000</TD>
                    <TD>1,024</TD>
                </TR>
                <TR align=center>
                    <TH>/53</TH>
                    <TD>37,778,931,862,957,100,000,000</TD>
                    <TD>2,048</TD>
                </TR>
                <TR align=center>
                    <TH>/52</TH>
                    <TD>75,557,863,725,914,300,000,000</TD>
                    <TD>4,096</TD>
                </TR>
                <TR align=center>
                    <TH>/51</TH>
                    <TD>151,115,727,451,828,000,000,000</TD>
                    <TD>8,192</TD>
                </TR>
                <TR align=center>
                    <TH>/50</TH>
                    <TD>302,231,454,903,657,000,000,000</TD>
                    <TD>16,384</TD>
                </TR>
                <TR align=center>
                    <TH>/49</TH>
                    <TD>604,462,909,807,314,000,000,000</TD>
                    <TD>32,768</TD>
                </TR>
                <TR align=center bgcolor=yellow>
                    <TH>/48</TH>
                    <TD>1,208,925,819,614,620,000,000,000</TD>
                    <TD>65,536 This is the standard business allocation</TD>
                </TR>
                <TR align=center>
                    <TH>/47</TH>
                    <TD>2,417,851,639,229,250,000,000,000</TD>
                    <TD>131,072</TD>
                </TR>
                <TR align=center>
                    <TH>/46</TH>
                    <TD>4,835,703,278,458,510,000,000,000</TD>
                    <TD>262,144</TD>
                </TR>
                <TR align=center>
                    <TH>/45</TH>
                    <TD>9,671,406,556,917,030,000,000,000</TD>
                    <TD>524,288</TD>
                </TR>
                <TR align=center>
                    <TH>/44</TH>
                    <TD>19,342,813,113,834,000,000,000,000</TD>
                    <TD>1,048,576</TD>
                </TR>
                <TR align=center>
                    <TH>/43</TH>
                    <TD>38,685,626,227,668,100,000,000,000</TD>
                    <TD>2,097,152</TD>
                </TR>
                <TR align=center>
                    <TH>/42</TH>
                    <TD>77,371,252,455,336,200,000,000,000</TD>
                    <TD>4,194,304</TD>
                </TR>
                <TR align=center>
                    <TH>/41</TH>
                    <TD>154,742,504,910,672,000,000,000,000</TD>
                    <TD>8,388,608</TD>
                </TR>
                <TR align=center>
                    <TH>/40</TH>
                    <TD>309,485,009,821,345,000,000,000,000</TD>
                    <TD>16,777,216</TD>
                </TR>
                <TR align=center>
                    <TH>/39</TH>
                    <TD>618,970,019,642,690,000,000,000,000</TD>
                    <TD>33,554,432</TD>
                </TR>
                <TR align=center>
                    <TH>/38</TH>
                    <TD>1,237,940,039,285,380,000,000,000,000</TD>
                    <TD>67,108,864</TD>
                </TR>
                <TR align=center>
                    <TH>/37</TH>
                    <TD>2,475,880,078,570,760,000,000,000,000</TD>
                    <TD>134,217,728</TD>
                </TR>
                <TR align=center>
                    <TH>/36</TH>
                    <TD>4,951,760,157,141,520,000,000,000,000</TD>
                    <TD>268,435,456</TD>
                </TR>
                <TR align=center>
                    <TH>/35</TH>
                    <TD>9,903,520,314,283,040,000,000,000,000</TD>
                    <TD>536,870,912</TD>
                </TR>
                <TR align=center>
                    <TH>/34</TH>
                    <TD>19,807,040,628,566,000,000,000,000,000</TD>
                    <TD>1,073,741,824</TD>
                </TR>
                <TR align=center>
                    <TH>/33</TH>
                    <TD>39,614,081,257,132,100,000,000,000,000</TD>
                    <TD>2,147,483,648</TD>
                </TR>
                <TR align=center bgcolor=yellow>
                    <TH>/32</TH>
                    <TD>79,228,162,514,264,300,000,000,000,000</TD>
                    <TD>4,294,967,296 This is the standard ISP Allocation</TD>
                </TR>
                <TR align=center>
                    <TH>/31</TH>
                    <TD>158,456,325,028,528,000,000,000,000,000</TD>
                    <TD>8,589,934,592</TD>
                </TR>
                <TR align=center>
                    <TH>/30</TH>
                    <TD>316,912,650,057,057,000,000,000,000,000</TD>
                    <TD>17,179,869,184</TD>
                </TR>
                <TR align=center>
                    <TH>/29</TH>
                    <TD>633,825,300,114,114,000,000,000,000,000</TD>
                    <TD>34,359,738,368</TD>
                </TR>
                <TR align=center>
                    <TH>/28</TH>
                    <TD>1,267,650,600,228,220,000,000,000,000,000</TD>
                    <TD>68,719,476,736</TD>
                </TR>
                <TR align=center>
                    <TH>/27</TH>
                    <TD>2,535,301,200,456,450,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/26</TH>
                    <TD>5,070,602,400,912,910,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/25</TH>
                    <TD>10,141,204,801,825,800,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/24</TH>
                    <TD>20,282,409,603,651,600,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/23</TH>
                    <TD>40,564,819,207,303,300,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/22</TH>
                    <TD>81,129,638,414,606,600,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/21</TH>
                    <TD>162,259,276,829,213,000,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/20</TH>
                    <TD>324,518,553,658,426,000,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/19</TH>
                    <TD>649,037,107,316,853,000,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/18</TH>
                    <TD>1,298,074,214,633,700,000,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/17</TH>
                    <TD>2,596,148,429,267,410,000,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/16</TH>
                    <TD>5,192,296,858,534,820,000,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/15</TH>
                    <TD>10,384,593,717,069,600,000,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/14</TH>
                    <TD>20,769,187,434,139,300,000,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/13</TH>
                    <TD>41,538,374,868,278,600,000,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/12</TH>
                    <TD>83,076,749,736,557,200,000,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/11</TH>
                    <TD>166,153,499,473,114,000,000,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/10</TH>
                    <TD>332,306,998,946,228,000,000,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/9</TH>
                    <TD>664,613,997,892,457,000,000,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
                <TR align=center>
                    <TH>/8</TH>
                    <TD>1,329,227,995,784,910,000,000,000,000,000,000,000</TD>
                    <TD></TD>
                </TR>
            </TABLE>
        </div>
    </div>
</section>

<!-- JS -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/all.min.js"></script>

<footer class="container-fluid margin-top-05">
    <div class="row">
        <div class="col-auto mx-auto">
            <p>Copyright &copy; <?= date("Y"); ?> <a href="https://www.maxtpower.com">Maxwell Power</a></p>
        </div>
    </div>
</footer>

</body>
</html>